package Client;

public class Main {
    public static void main(String[] args) {
        System.out.println("Our project started!");

        //Try to connect to server
        TalkToServer.getInstance().handshakeServer();

        //Creating GUI and Bring the Login page up
        SignWindowGUI.getInstance().firstLoginWindow();

//        P2PClientGUI test = new P2PClientGUI("hello");
    }

}
