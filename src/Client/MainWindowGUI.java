package Client;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;


public class MainWindowGUI {

    private JFrame mainFrame = new JFrame("Chat Application");
    private JLabel welcome = new JLabel("Welcome to Chat App",JLabel.CENTER);

    private JPanel bodyPanel = new JPanel();
    private JPanel southPanel = new JPanel();

    private JPanel leftPanel = new JPanel();
    private JPanel rightPanel = new JPanel();

    private JPanel leftSouthPanel = new JPanel();
    private JPanel rightSouthPanel = new JPanel();

    private JTextArea groupChatArea = new JTextArea();
    private JScrollPane groupChatScroll = new JScrollPane(groupChatArea);

    private JTextArea usersListTextArea = new JTextArea();
    private JScrollPane addedUserScroll = new JScrollPane(usersListTextArea);

    private JButton startP2PChatButton = new JButton("Start Chat with");
    private JButton sendGroupChatButton = new JButton("Send");
    private JTextField p2pChatTextField = new JTextField();
    private JTextField groupChatTextField = new JTextField();
    private JLabel footer = new JLabel("This is just footer",JLabel.CENTER);
    private JLabel blankLabel1 = new JLabel("           ");
    private JLabel blankLabel2 = new JLabel("           ");


    private String username;
    private ListenToServer listen = new ListenToServer();
    private HashMap<String,P2PClientGUI> p2pThreadsMap = new HashMap<>();


    public MainWindowGUI(String username){
        this.username=username;
        createMainWindow();
        this.listen.start();

        TalkToServer.getInstance().gChatHistReq("GROUPCHAT");
    }


    private void createMainWindow(){
        int WIDTH = 700;
        int HEIGHT = 500;
        Point center = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint(); // get center point

        mainFrame.setLayout(new BorderLayout(20,20));
        mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        mainFrame.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                if (JOptionPane.showConfirmDialog(mainFrame,
                        "Are you sure you want to close this window?", "Close Window?",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION)
                {
                    System.exit(0);
                }

            }
        });
        mainFrame.setSize(WIDTH, HEIGHT);
        mainFrame.setLocation(50 , 50);
        mainFrame.setBounds(center.x - WIDTH / 2, center.y - HEIGHT / 2, WIDTH, HEIGHT); // set frame to center
        mainFrame.setMinimumSize(new Dimension(WIDTH - 100, HEIGHT - 50));

        bodyPanel.setSize(50,40);
        bodyPanel.setLayout(new BorderLayout());

        footer.setText("You are logged in as " + username);
        southPanel.add(footer);

        usersListTextArea.setSize(180,400);
        usersListTextArea.setLineWrap(true);

        groupChatArea.setSize(250,400);
        groupChatArea.setLineWrap(true);

        leftPanel.setLayout(new BorderLayout());
        leftPanel.setSize(200, 400);
        leftPanel.add(addedUserScroll,BorderLayout.CENTER);
        leftSouthPanel.setLayout(new GridLayout(2,1));
        leftSouthPanel.add(p2pChatTextField);
        leftSouthPanel.add(startP2PChatButton);
        leftPanel.add(leftSouthPanel,BorderLayout.SOUTH);

        rightPanel.setLayout(new BorderLayout());
        rightPanel.setSize(300, 400);
        rightPanel.add(groupChatScroll);
        rightSouthPanel.setLayout(new GridLayout(2,1));
        rightSouthPanel.add(groupChatTextField);
        rightSouthPanel.add(sendGroupChatButton);
        rightPanel.add(rightSouthPanel,BorderLayout.SOUTH);

        usersListTextArea.setEditable(false);
        groupChatArea.setEditable(false);

        bodyPanel.add(leftPanel,BorderLayout.WEST);
        bodyPanel.add(rightPanel,BorderLayout.EAST);

        startP2PChatButton.addActionListener(event -> p2pChatButtonHandler(p2pChatTextField.getText()));
        p2pChatTextField.addActionListener(event -> p2pChatButtonHandler(p2pChatTextField.getText()));
        sendGroupChatButton.addActionListener(event -> sendGroupChat());
        groupChatTextField.addActionListener(event -> sendGroupChat());


        mainFrame.add(bodyPanel,BorderLayout.CENTER);
        mainFrame.add(welcome,BorderLayout.NORTH);
        mainFrame.add(footer,BorderLayout.SOUTH);
        mainFrame.add(blankLabel1,BorderLayout.WEST);
        mainFrame.add(blankLabel2,BorderLayout.EAST);

        mainFrame.setVisible(true);
    }

    private void sendGroupChat() {
        if (!groupChatTextField.getText().equals("")) {
            TalkToServer.getInstance().sendGroupChat(groupChatTextField.getText());
            JScrollBar sb =groupChatScroll.getVerticalScrollBar();
            sb.setValue( sb.getMaximum()+1);
            groupChatTextField.setText("");
        }
    }

    private void p2pChatButtonHandler(String otherPair){
        String[] list = usersListTextArea.getText().split("\n");
        for (String s : list) {
            if ((s.equals(otherPair) && !otherPair.equals(this.username))) { // check username is in the registered list
                if (p2pThreadsMap.getOrDefault(otherPair, null) == null) { // check if there is a thread already running
                    //start new thread
                    P2PClientGUI newPrivateChat = new P2PClientGUI(this.username, otherPair);
                    newPrivateChat.start();
                    p2pThreadsMap.put(otherPair, newPrivateChat);
                    return;
                } else if (p2pThreadsMap.get(otherPair).isHidden()) {
                    p2pThreadsMap.get(otherPair).setMainFrameVisible();
                    return;
                } else {
                    JOptionPane.showMessageDialog(mainFrame, "There is already a chat window open \nwith this user.");
                    return;
                }
            }
        }
        JOptionPane.showMessageDialog(mainFrame, "This is not a registered user.");
    }

    private class ListenToServer extends Thread {
        public ListenToServer() {
        }
        @Override
        public void run() {
            while (true) {
                String[] str = TalkToServer.getInstance().serverListener();
                switch (str[0]) {
                    case "GROUPCHATHIST":
                        if (str[1] == null) {
                            break;
                        }
                        groupChatArea.append(str[1] + ": " + str[2]);
                        groupChatArea.append("\n");
                    break;

                    case "GROUPCHAT" : //GROUPCHAT + message
                        groupChatArea.append(str[1]);
                        groupChatArea.append("\n");
                    break;

                    case "USERLIST": // USERLIST + String[]
                        usersListTextArea.setText("");
                        for (String user : str[1].split("-")) {
                            usersListTextArea.append(user);
                            usersListTextArea.append("\n");
                        }
                    break;

                    case "PCHAT" : // PCHAT + sender + message
                        String[] s = str[1].split("-"); // splited into sender's name and message
                        if (p2pThreadsMap.getOrDefault(s[0], null) != null) {
                            p2pThreadsMap.get(s[0]).appendToChatArea(s[0],s[1]);
                        }
                    break;

                    case "PCHATHIST": //PCHATHIST + otherpair + sender:messgae
                        if (str[1] == null) {
                            return;
                        }
                        if (p2pThreadsMap.getOrDefault(str[1], null) != null) {
                            if(str[2] != null && str[3] != null) {
                                p2pThreadsMap.get(str[1]).appendToChatArea(str[2],str[3]);
                            }
                        }
                    break;

                    case "BUZZ" :// BUZZ + sender
                        if (p2pThreadsMap.getOrDefault(str[1], null) != null) {
                            p2pThreadsMap.get(str[1]).appendToChatArea(str[1], "BUZZ");
                            p2pThreadsMap.get(str[1]).makeItBuzz();
                        }
                }
            }
        }
    }
}





