package Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowListener;
import java.util.concurrent.TimeUnit;

public class P2PClientGUI extends Thread{


    private JFrame mainFrame = new JFrame("Chat Application");
    private JLabel header = new JLabel(" Private Chat ", JLabel.CENTER);

    private JPanel bodyPanel = new JPanel();
    private JPanel southPanel = new JPanel();

    private JTextArea privateChatArea = new JTextArea();
    private JScrollPane privateChatScroll = new JScrollPane(privateChatArea);

    private JButton sendButton = new JButton("Send");
    private JButton buzzButton = new JButton("BUZZ");
    private JTextField chatTextField = new JTextField(15);

    private JLabel footer = new JLabel("", JLabel.CENTER);
    private JLabel blankLabel1 = new JLabel("           ");
    private JLabel blankLabel2 = new JLabel("           ");

    private String otherPair;
    private String username;
    private boolean canBuzz = false;
    private final int WIDTH = 550;
    private final int HEIGHT = 400;
    private final Point center = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint(); // get center point

    //constructor
    public P2PClientGUI(String username, String otherPair) {
        this.username = username;
        this.otherPair = otherPair;
        TalkToServer.getInstance().pChatHistReq(otherPair);
    }


    @Override
    public void run() {
        Point center = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint(); // get center point

        mainFrame.setLayout(new BorderLayout(20, 20));
        mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        mainFrame.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {

                    mainFrame.setVisible(false);

            }
        });        mainFrame.setSize(WIDTH, HEIGHT);
        mainFrame.setBounds(center.x - WIDTH / 2, center.y - HEIGHT / 2, WIDTH, HEIGHT); // set frame to center
        mainFrame.setMinimumSize(new Dimension(WIDTH , HEIGHT ));

        bodyPanel.setSize(400, 200);
        bodyPanel.setLayout(new BorderLayout());
        bodyPanel.add(privateChatScroll, BorderLayout.CENTER);

        footer.setText("You are chatting with " + otherPair);

        privateChatArea.setSize(400, 200);
        privateChatArea.setLineWrap(true);

        southPanel.add(chatTextField);
        southPanel.add(sendButton);
        southPanel.add(buzzButton);
        privateChatArea.setEditable(false);

        bodyPanel.add(privateChatScroll, BorderLayout.CENTER);
        bodyPanel.add(southPanel, BorderLayout.SOUTH);

        mainFrame.add(bodyPanel, BorderLayout.CENTER);
        mainFrame.add(header, BorderLayout.NORTH);
        mainFrame.add(footer, BorderLayout.SOUTH);
        mainFrame.add(blankLabel1, BorderLayout.WEST);
        mainFrame.add(blankLabel2, BorderLayout.EAST);
        mainFrame.setVisible(true);

        // Action Listener
        sendButton.addActionListener(event -> sendMessage(chatTextField.getText(), otherPair));
        chatTextField.addActionListener(event -> sendMessage(chatTextField.getText(), otherPair));
        buzzButton.addActionListener(event -> sendBuzz());
    }

    public boolean isHidden() {
        return !this.mainFrame.isVisible();
    }

    public void setMainFrameVisible() {
        this.mainFrame.setVisible(true);
    }


    /**
     * Send message to other peer.
     * @param message
     * @param receiver
     */
    private void sendMessage(String message, String receiver) {
        if (!message.equals("")) {
            TalkToServer.getInstance().sendPrivateChat(message, receiver);
            privateChatArea.append( username + ": " + message);
            privateChatArea.append("\n");
            chatTextField.setText("");
        }
    }

    /**
     * Send buzz to other peer.
     */
    private void sendBuzz() {
        if(this.canBuzz) {
            TalkToServer.getInstance().sendBuzz(otherPair);
            this.canBuzz = false;
        }
    }



    public void appendToChatArea(String sender , String message) {
        this.canBuzz = true;
        this.privateChatArea.append(sender + ": " + message);
        this.privateChatArea.append("\n");

        JScrollBar sb =privateChatScroll.getVerticalScrollBar();
        sb.setValue( sb.getMaximum()+1);
    }

    public void makeItBuzz() {
        mainFrame.toFront();
        for (int i = 0; i <5 ; i++) {
            for (int j = 0; j <3 ; j++) {
                mainFrame.setBounds(center.x - WIDTH / 2-j*10, center.y - HEIGHT / 2-j*10, WIDTH, HEIGHT); // set frame to center
                mainFrame.repaint();
                try {
                    TimeUnit.MILLISECONDS.sleep(100);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            Toolkit.getDefaultToolkit().beep();
        }
    }

    public void loadHistory() {

    }
}


