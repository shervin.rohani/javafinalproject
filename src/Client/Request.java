package Client;


/**
 * This class handles the request, it actually works as a request/response class.
 * (similar to http request)
 */
public class Request {


    // Attributes
    private Enum header;
    private String sender;
    private String receiver;
    private String Content;


    // Constructor
    public Request(Enum header, String sender, String receiver, String content) {
        this.header = header;
        this.sender = sender;
        this.receiver = receiver;
        Content = content;
    }

    // Getter and Setter
    public Enum getHeader() {
        return header;
    }

    public void setHeader(Enum header) {
        this.header = header;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }
}
