package Client;

import javax.swing.*;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Arrays;

public class SignWindowGUI {
    // Constants
    private final String CHAT_APPLICATION = "Chat Application";
    private final String WELCOME_TO_CHATAPP = "Welcome to Chat App";
    private final String SIGN_UP = "Sign Up";
    private final String SIGN_IN = "Sign In";
    private final String USERNAME = "Username";
    private final String PASSWORD = "Password";
    private final String CONFIRM_PASSWORD = "Confirm Pass";
    private final String ALREADY_HAVE_AN_ACCOUNT = "Already have an acount?";
    private final String NEED_AN_ACCOUNT = "Need an account?";
    private final String BLANK_STRING = "            ";
    private final String SANS_FONT = "SansSerif";

    private final int WIDTH = 300;
    private final int HEIGHT = 400;

    static final String SERVER_IP = "localhost"; // Determine the server IP here.


    private JFrame mainFrame = new JFrame(CHAT_APPLICATION);
    private JLabel welcome = new JLabel(WELCOME_TO_CHATAPP,JLabel.CENTER);

    private JButton signUpButtom = new JButton(SIGN_UP);
    private JButton loginButton = new JButton(SIGN_IN);
    private JButton signUpInLoginButtom = new JButton(SIGN_UP);
    private JButton loginInSignupButton = new JButton(SIGN_IN);

    private JLabel usernameLable = new JLabel(USERNAME);
    private JLabel passLabel = new JLabel(PASSWORD);
    private JLabel rePassLabel = new JLabel(CONFIRM_PASSWORD);
    private JLabel hasAccountLabel = new JLabel(ALREADY_HAVE_AN_ACCOUNT);
    private JLabel needNewAccountLabel = new JLabel(NEED_AN_ACCOUNT);
    private JLabel blankLabel1 = new JLabel(BLANK_STRING);
    private JLabel blankLabel2 = new JLabel(BLANK_STRING);

    private JPasswordField passField = new JPasswordField(20);
    private JPasswordField rePassField = new JPasswordField(20);
    private JTextField usernameField = new JTextField(20);

    private JPanel bodyPanel = new JPanel();
    private JPanel southPanel = new JPanel();

    private MainWindowGUI mainWindowGUI;

    // Singleton Pattern
    private static SignWindowGUI signWindowGUI = new SignWindowGUI();

    public static SignWindowGUI getInstance() {
        return signWindowGUI;
    }



    //Constructor
    private SignWindowGUI(){
        Point center = GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint(); // get center point

        this.mainFrame.setLayout(new BorderLayout(20, 20));
        this.mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.mainFrame.setSize(WIDTH, HEIGHT);
        this.mainFrame.setLocation(50, 50);
        this.mainFrame.setBounds(center.x - WIDTH / 2, center.y - HEIGHT / 2, WIDTH, HEIGHT); // set frame to center
        this.mainFrame.setMinimumSize(new Dimension(WIDTH - 50, HEIGHT - 25));

        this.bodyPanel.setLayout(new GridLayout(8, 1, 10, 10));
        this.bodyPanel.setSize(50, 40);
        this.bodyPanel.add(usernameLable);
        this.bodyPanel.add(usernameField);
        this.bodyPanel.add(passLabel);
        this.bodyPanel.add(passField);
        this.bodyPanel.add(loginButton);

        this.southPanel.add(needNewAccountLabel);
        this.southPanel.add(signUpInLoginButtom);

        this.loginInSignupButton.addActionListener(event -> signupToLogin());
        this.signUpInLoginButtom.addActionListener(event -> loginToSignup());
        this.loginButton.addActionListener(event -> loginHandler());
        this.passField.addActionListener(event -> loginHandler());
        this.signUpButtom.addActionListener(event -> signupHandler());
        this.rePassField.addActionListener(event -> signupHandler());

        this.welcome.setFont(new Font(SANS_FONT, Font.BOLD, 16));
        this.welcome.setPreferredSize(new Dimension(100, 50));

        this.mainFrame.add(welcome, BorderLayout.NORTH);
        this.mainFrame.add(southPanel, BorderLayout.SOUTH);
        this.mainFrame.add(bodyPanel, BorderLayout.CENTER);
        this.mainFrame.add(blankLabel1, BorderLayout.WEST);
        this.mainFrame.add(blankLabel2, BorderLayout.EAST);
    }



     public void firstLoginWindow() {
         this.mainFrame.setVisible(true);
    }



     private void loginToSignup(){

        bodyPanel.remove(loginButton);
        bodyPanel.add(rePassLabel);
        bodyPanel.add(rePassField);
        bodyPanel.add(signUpButtom);

        southPanel.removeAll();
        southPanel.add(hasAccountLabel);
        southPanel.add(loginInSignupButton);
        bodyPanel.revalidate();
        bodyPanel.repaint();
        mainFrame.getContentPane().repaint();

    }

    private  void signupToLogin(){

        bodyPanel.remove(rePassField);
        bodyPanel.remove(rePassLabel);
        bodyPanel.remove(signUpButtom);
        bodyPanel.add(loginButton);

        southPanel.removeAll();
        southPanel.add(needNewAccountLabel);
        southPanel.add(signUpInLoginButtom);
        bodyPanel.revalidate();
        bodyPanel.repaint();
        mainFrame.getContentPane().repaint();
    }

    private void signupHandler(){

        boolean signupSuccess;
        signupSuccess = TalkToServer.getInstance().signUpTry(usernameField.getText(),passField.getPassword(),
                rePassField.getPassword(),mainFrame);
        if (signupSuccess){
            mainFrame.setVisible(false);
            this.mainWindowGUI = new MainWindowGUI(usernameField.getText());

        }

    }

    private void loginHandler(){
        boolean loginSuccess;
        loginSuccess = TalkToServer.getInstance().loginTry(usernameField.getText(),passField.getPassword(), mainFrame);
        if (loginSuccess){
            mainFrame.setVisible(false);
            mainWindowGUI = new MainWindowGUI(usernameField.getText());

        }

    }

    public MainWindowGUI getMainWindowGUI() {
        return mainWindowGUI;
    }
}


