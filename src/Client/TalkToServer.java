package Client;

import Server.Headers;

import javax.swing.*;
import java.io.*;
import java.net.Socket;
import java.util.Arrays;

public class TalkToServer {

    private static TalkToServer ourInstance = new TalkToServer();

    private static final String SERVER_IP = "95.156.255.6";
    private Socket socket;
    private BufferedReader socketInput;
    private PrintWriter socketOutput;

    public static TalkToServer getInstance() {
        return ourInstance;
    }

    private TalkToServer() {

    }


    public void handshakeServer(){
        try {

            this.socket = new Socket(SERVER_IP, 9090);
            this.socketInput = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.socketOutput = new PrintWriter(socket.getOutputStream(), true);

        } catch (Exception e){
            JOptionPane.showMessageDialog(null,"Could not connect to the server!! Exiting now...");
            System.exit(0);
        }
    }

    public  boolean loginTry(String username, char[] password, JFrame mainFrame){


        //----------------Verifying the Password and otherPair in the client side-----------------------------
        if (password.length == 0) {// if no password was entered...
            JOptionPane.showMessageDialog(mainFrame, "At least enter something for Gods sakes!");
            return false;
        }

        if (username.isEmpty()){// if no otherPair was entered
            JOptionPane.showMessageDialog(mainFrame, "Enter the Username");
            return false;
        }

        //------------------------ Connecting to server and Verifying the otherPair -------------------------


        socketOutput.println(Headers.SIGNIN);// telling the server that a login process is required.
        socketOutput.println(username);// Sending otherPair to Server
        socketOutput.println(password);// Sending password to Server

        try {
            if (socketInput.readLine().equals("WrongUserOrPass")){// if user name or password is wrong
                JOptionPane.showMessageDialog(mainFrame, "Username or password is wrong");
                return false;
            }
        } catch (IOException e) { //Could not read line from socket
            e.printStackTrace();
            JOptionPane.showMessageDialog(mainFrame, "Can not connect to the Server.\n Please try again");
            return false;
        }

        return true;


    }

    public boolean signUpTry(String username, char[] pass , char[] repass, JFrame mainFrame){



        //----------------Verifying the Password and otherPair in the client side-----------------------------
        if (pass.length == 0) {// if no password was entered...
            JOptionPane.showMessageDialog(mainFrame, "At least enter something for Gods sakes!");
            return false;
        }

        if (!(pass.length==repass.length)) {// if the length of 2 passwords are not equal.
            JOptionPane.showMessageDialog(mainFrame, "Passwords do not match...Try again");
            return false;
        }
        if (!Arrays.equals(pass,repass)) {// if 2 passwords do not match.
            JOptionPane.showMessageDialog(mainFrame, "Passwords do not match...Try again");
            return false;
        }


        if (username.isEmpty()){// if no otherPair was entered
            JOptionPane.showMessageDialog(mainFrame, "Enter the Username");
            return false;
        }

        //------------------------ Connecting to server and Verifying the otherPair -------------------------

        socketOutput.println(Headers.SIGNUP);// telling the server that a sign up process is required.
        socketOutput.println(username);// Sending otherPair to Server for checking

        try {
            if (socketInput.readLine().equals("UserNameExist")){// if user name already exist in the server
                JOptionPane.showMessageDialog(mainFrame, "Username exist. Try another otherPair.");
                return false;
            }
        } catch (IOException e) { //Could not read line from socket
            e.printStackTrace();
            JOptionPane.showMessageDialog(mainFrame, "Can not connect to the Server.\n Please try again");
            return false;
        }

        socketOutput.println(pass);
        return true;
    }



    public void sendGroupChat(String message) {
        socketOutput.println("GROUPCHAT,,," + message);

    }


    public String[] serverListener() {
        try {
            String[] str = socketInput.readLine().split(",,,");
            return str;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public void sendPrivateChat(String message, String receiver) {
        socketOutput.println("PCHAT" + ",,," + receiver + ",,," + message);
    }

    public void sendBuzz(String receiver) {
        socketOutput.println("BUZZ" + ",,," + receiver);
    }

    //TODO a thread which run in private chat and listen for incoming messages
    //TODO method which send private chat to server
    //TODO method which check if a otherPair exist fot private chat

    public void gChatHistReq(String receiver) {
        socketOutput.println("GROUPCHATHIST" + ",,," + receiver);
    }

    public void pChatHistReq(String otherPair) {
        socketOutput.println("PCHATHIST" + ",,," + otherPair);
    }
}
