package Server;

import java.util.ArrayList;

public class Authenticate {

    public static boolean loginAuthenticate(String username, String password) {


        return RegisteredUsers.getInstance().getUserPassMap().containsKey(username) &&
                RegisteredUsers.getInstance().getUserPassMap().get(username).equals(password);
    }

    public static boolean signUpAuthenticate(String username, String password) {



        return false;

    }

    public static boolean isUsernameUsed(String username) {

        return RegisteredUsers.getInstance().getUserPassMap().containsKey(username);


    }
}
