package Server;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.Random;

/**
 * This class provides chat between different.
 * It's the super class of p2pChat and groupChat
 */
public abstract class Chat {
    // Constant fields
    private final int RANDOM_RANGE = 100000; // 6 digit number

    // Attributes
    private History history; // Each chat has a corresponding history object.
    private int id; // Each chat has a id

    // Constructor
    public Chat() {
        this.history = new History();

        // Generate a random number
        Random rand = new Random();

        this.id = rand.nextInt(RANDOM_RANGE);
    }

    // Methods
    /**
     * Send message from a user to another
     * @param sender user who sends the message
     * @param reciver user who receives the message
     * @param reader buffered reader which is used to read message
     * @param writer print writer which is used to send message
     * @return
     * @throws Exception
     */
    public String sendMsg(User sender, User reciver, BufferedReader reader, PrintWriter writer) throws Exception{
        while (true) {
            // Receive message from reader
            String message = reader.readLine();

            // Check the message content. (It's null or empty or only spaces)
            if (message == null || message.trim().length() == 0) {
                continue;
            }

            // Send message to receiver
            writer.println(sender.getUsername() + " @ " + LocalDateTime.now().toString() + message);

            // Add message to history
            history.addToHistory(sender.getUsername(), message);
        }
    }
}
