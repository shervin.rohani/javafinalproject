package Server;

import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * This class extends chat abstract class.
 * We only have one group chat so we use singleton pattern.
 */
public class ChatController {
    // Final attributes

    // Attributes
    ArrayList<User> groupChatUsers;

    // Singleton Stuffs
    private static ChatController ourInstance = new ChatController();

    public static ChatController getInstance() {
        return ourInstance;
    }

    // Constructor
    private ChatController() {
        this.groupChatUsers = RegisteredUsers.getInstance().getUsers();
    }

    // Group Chat users Setter
    public void setGroupChatUsers(ArrayList<User> groupChatUsers) {
        this.groupChatUsers = groupChatUsers;
    }

    // Methods
    public void sendToAll(String message,String sender) {
        // Update group chat users
        // Broadcast message to all
        for (User user:RegisteredUsers.getInstance().getUsers()) {
            PrintWriter pw = null;
            if (user.getUserSocket() != null ) {
                try {
                    pw = new PrintWriter(user.getUserSocket().getOutputStream(), true);
                    pw.println( "GROUPCHAT,,," + sender + ": " + message);
                } catch (Exception e) {
                    e.printStackTrace();
                    continue;
                }
            }
        }
        // Save message to history
        Database.addToHistory(sender, "GROUPCHAT", message);
    }

    /**
     * Send message to a specific user.
     * @param receiver
     * @param sender
     * @param message
     */
    public void sendToUser(String receiver, String sender, String message) {
        for (User user: RegisteredUsers.getInstance().getUsers()) {
            if (user.getUsername().equals(receiver)){
                // add to history
                Database.addToHistory(sender, receiver, message);

                // send message
                PrintWriter pw = null;
                try {
                    pw = new PrintWriter(user.getUserSocket().getOutputStream(), true);
                    pw.println( "PCHAT,,," + sender + "-" + message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return;
            }
        }
    }

    public void sendToUserBuzz(String receiver , String sender) {
        for (User user: RegisteredUsers.getInstance().getUsers()) {
            if (user.getUsername().equals(receiver)) {
                PrintWriter pw = null;
                try {
                    pw = new PrintWriter(user.getUserSocket().getOutputStream(), true);
                    pw.println("BUZZ,,," + sender);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return;
            }
        }
    }


}
