package Server;

import java.io.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * This class have static methods and fields and handle data io.
 */
public class Database {
    // Constant fields
    private static final String REGISTERED_USERS_PATH = "registeredUsers.txt";

    /**
     * Write registered users arraylist to a file.
     * @param usersMap
     */
    public static void writeRegisteredUsers(HashMap<String, String> usersMap) {
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        System.out.println("I'm in write registered users");
        try {
            fos = new FileOutputStream(REGISTERED_USERS_PATH);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(usersMap);
            oos.close();
            fos.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Load registered users from file.
     * @return
     */
    public static HashMap<String, String> loadRegisteredUsers() {
        HashMap<String, String> usersMap= new HashMap<>();

        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = new FileInputStream(REGISTERED_USERS_PATH);
            ois = new ObjectInputStream(fis);
            usersMap = (HashMap<String, String>) ois.readObject();
            ois.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return usersMap;
    }

    public static ArrayList<String> loadHistory(String sender, String receiver) {
        // Placeholder for result
        ArrayList<String> history = new ArrayList<>();

        String fileName = new String();

        // Check if we want group chat history
        if (sender.equals("GROUPCHAT") || receiver.equals("GROUPCHAT")) {
            fileName = "GROUPCHAT.txt";
        } else {
            fileName = (sender.compareTo(receiver) < 0) ? (sender + "_" + receiver + ".txt"): (receiver + "_" + sender + ".txt");
        }

        // Define a file path
        String filePath = new File("").getAbsolutePath();
        filePath = filePath.concat(File.separator + "src" + File.separator + "ChatHistory" + File.separator + fileName);

        // Read file line by line
        BufferedReader reader = null;

        File file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            reader = new BufferedReader(new FileReader(filePath));
            String line;
            while ((line = reader.readLine()) != null) {
                history.add(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return history;
    }

    /**
     * Add chat content to history
     * @param sender
     * @param receiver
     * @param message
     */
    public static void addToHistory(String sender, String receiver, String message) {
        String fileName = new String();
        // Check if we want group chat history
        if (sender.equals("GROUPCHAT") || receiver.equals("GROUPCHAT")) {
            fileName = "GROUPCHAT.txt";
        } else {
            fileName = (sender.compareTo(receiver) < 0) ? (sender + "_" + receiver + ".txt") : (receiver + "_" + sender + ".txt");
        }

        // Define a file path
        String filePath = new File("").getAbsolutePath();
        filePath = filePath.concat(File.separator + "src" + File.separator + "ChatHistory" + File.separator + fileName);

        // Write to file
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(new BufferedWriter(new FileWriter(new File(filePath), true)), true);
            writer.println(sender + ",,," + message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


