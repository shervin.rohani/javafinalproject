package Server;

public enum Headers {
    SIGNUP,
    SIGNIN,
    ONLINEUSER,
    GROUPCHAT,
    PCHAT,
    BUZZ,
    PCHATHIST,
    GROUPCHATHIST
}

