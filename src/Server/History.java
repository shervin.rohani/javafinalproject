package Server;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * This class hold the history of each chat.
 */
public class History {
    // Constant fields
    private final int HISTORY_LENGTH = 50; // number of history lines which will be saved.


    // Attributes
    private Map<String, String> historyMap; // a map of (k, v) = (username + date, message)


    // Constructor
    public History() {
        this.historyMap = new HashMap<>();
    }

    // Methods

    /**
     * Add a messeage to history and take care about the history length.
     * @param userName
     * @param message
     */
    public void addToHistory(String userName, String message) {

        historyMap.put(userName + ",,," + LocalDateTime.now().toString(), message);

        // Keep the history map size fixed.
        while (historyMap.size() > HISTORY_LENGTH) {
            String keyToRemove = historyMap.get(historyMap.keySet().toArray()[0]); // get the first key
            historyMap.remove(keyToRemove);
        }
    }

    /**
     * Returns the chat history.
     * @return
     */
    public Map<String, String> getHistoryMap() {
        return historyMap;
    }
}
