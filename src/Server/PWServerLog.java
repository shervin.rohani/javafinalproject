package Server;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.time.LocalDateTime;

public class PWServerLog {

    private static PWServerLog ourInstance = new PWServerLog();

    private  PrintWriter pwServerLog;


    public static PWServerLog getInstance() {
        return ourInstance;
    }

    private PWServerLog() {

        try{
            pwServerLog= new PrintWriter(new BufferedWriter(new FileWriter( "serverlog.txt",true)),true);
        }catch (Exception e){
            System.out.println("Can not create logger for server....exiting now");
            e.printStackTrace();
            System.exit(1);
        }
    }


    public void pDisconnect(Socket socket) {
        this.pwServerLog.println("Socket with port " + socket.getPort() + " was disconnected at" + LocalDateTime.now());
    }
    public void pDisconnect(Socket socket , String username) {
        this.pwServerLog.println(username + " with port " + socket.getPort() + " was disconnected at" + LocalDateTime.now());
    }

    public void pLogin(String username, Socket socket) {
        this.pwServerLog.println("LogIn : " + username + " with " +
                socket.toString()+" has logged in at " + LocalDateTime.now().toString());
    }

    public void pSignUp(String username, Socket socket) {
        this.pwServerLog.println("SignUp : " + username + " with " +
                socket.toString()+" has signed up at " + LocalDateTime.now().toString());
    }

    public void pServerUp() {
        this.pwServerLog.println();
        this.pwServerLog.println("Server is up. Waiting for clients...  at " + LocalDateTime.now().toString());
    }

    public void pNewConnection(Socket socket) {
        this.pwServerLog.println("New Connection : " + socket.toString() + " at " + LocalDateTime.now().toString());
    }
}
