package Server;

import java.io.Serializable;
import java.util.*;

/**
 * This class holds the record of all registered users.
 * Only server has access to it and have permission to edit this.
 * We only have one RegisteredUsers list, so the singleton pattern is the key :D
 */
public class RegisteredUsers implements Serializable {
    // Attributes
    private ArrayList<User> users;
    private HashMap<String,String> userPassMap;

    // Singleton Stuffs
    private static RegisteredUsers registeredUsers = new RegisteredUsers();
    public static RegisteredUsers getInstance() {
        return registeredUsers;
    }

    // Private constructor
    private RegisteredUsers() {
        // load
        this.userPassMap = Database.loadRegisteredUsers();

        // create users
        this.users = new ArrayList<>();
        userPassMap.forEach((k,v) -> this.users.add(new User(k, v)));
    }

    // Methods
    /**
     * Add user to registered user. (noone has permission to remove users, so we wont have remove user method here)
     * @param registeredUser User who just signed up
     * @return true if it could add and fals if not.
     */
    public boolean addUser(User registeredUser) {
        try {
            this.users.add(registeredUser); // add to list
            this.userPassMap.put(registeredUser.getUsername(), registeredUser.getPassword()); // add to map

            Database.writeRegisteredUsers(this.userPassMap); // update registered users list.

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Returns all users arraylist.
     * @return
     */
    public ArrayList<User> getUsers() {
        return users;
    }

    public Map<String, String> getUserPassMap() {
        return userPassMap;
    }


    public User getUserByUsername(String username) {
        for (User user:users) {
            if (user.getUsername().equals(username)){
                return user;
            }
        }
        return null;
    }
}

