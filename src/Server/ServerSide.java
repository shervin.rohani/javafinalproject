package Server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class ServerSide {


    public static void main(String[] args) throws Exception{
        System.out.println("Waiting for clients... ");
        PWServerLog.getInstance().pServerUp();
        ServerSocket serverSocket = new ServerSocket(9090);

        while(true) {
            Socket soc = serverSocket.accept();
            System.out.println("Connection Established!");
            System.out.println(soc.toString() + " is connected");
            PWServerLog.getInstance().pNewConnection(soc);
            mainUserThread userThread = new mainUserThread(soc);
            userThread.start();
        }
    }

}



class mainUserThread extends Thread{
    // Attributes
    Socket socket;
    BufferedReader socketInput;
    PrintWriter socketOutput;
    String username;
    String password;

    public mainUserThread(Socket socket){
        this.socket=socket;
    }

    public void run(){
            //------------------ SignUp and Log in process. At the end, one user will be distinguished.---------------
        try {
            socketInput = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            socketOutput = new PrintWriter(socket.getOutputStream(),true);

            while_loop:
            while (true){
                switch (Headers.valueOf(socketInput.readLine())) {
                    case SIGNUP:
                        username = socketInput.readLine();

                        if (Authenticate.isUsernameUsed(username)) {// If username exist, return -1 to client
                            socketOutput.println("UserNameExist");
                        }else {//adding the new user to the RegisteredUsers and OnlineUsers
                            socketOutput.println("OK");
                            password= socketInput.readLine();


                            // Create a new user and set socket for it.
                            User connectedUser = new User(username,password);
                            connectedUser.setUserSocket(socket);

                            // Add to registered users
                            RegisteredUsers.getInstance().addUser(connectedUser);
                            PWServerLog.getInstance().pSignUp(username,socket);
                            socketOutput.println("OK");

                            //TODO user is now signed up. get the thread for main chat window
                            UserThread userThread = new UserThread(socket,username);
                            userThread.start();
                            break while_loop;
                        }
                        break;

                    case SIGNIN:
                        username= socketInput.readLine();
                        password = socketInput.readLine();

                        if (Authenticate.loginAuthenticate(username,password)){
                            socketOutput.println("LoggedIn");

                            PWServerLog.getInstance().pLogin(username,socket);

                            // Set socket for user
                            RegisteredUsers.getInstance().getUserByUsername(username).setUserSocket(socket);


                            //TODO(shr) must be edited. get the thread for main chat window
                            UserThread userThread = new UserThread(socket,username);
                            userThread.start();
                            break while_loop;
                        }
                        socketOutput.println("WrongUserOrPass");
                        break;
                }
            }

        }catch (Exception e){
            System.out.println("Socket with port " + socket.getPort() + " was disconnected at" + LocalDateTime.now());
            PWServerLog.getInstance().pDisconnect(socket);
            e.printStackTrace();
        }
    }
}