package Server;

import java.io.Serializable;
import java.net.Socket;

/**
 * This class will be used to instantiate user objects.
 */
public class User implements Serializable {
    // Constants
    private final String UNKNOWN = "UNK";

    // Attributes
    private String username;
    private String password;
    private int id;

    private Socket userSocket=null; //Should be initiate

    // Constructor
    public User(String username, String password) {
        this.username = username;
        this.password = password;
        this.id = 0;
    }



    // Getter and Setter
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUserSocket(Socket userSocket) {
        this.userSocket = userSocket;
    }

    public Socket getUserSocket() {
        return userSocket;
    }
}
