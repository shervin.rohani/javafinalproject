package Server;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class UserThread extends Thread {
    // Constant fields



    // Attributes
    private Socket socket;
    BufferedReader socketInput;
    PrintWriter socketOutput;
    String username;

    // Constructor
    public UserThread(Socket socket,String username) {
        this.socket = socket;
        this.username = username;
    }


    // Methods
    @Override
    public void run() {
        try {
            socketInput = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            socketOutput = new PrintWriter(socket.getOutputStream(),true);

            //Sending all users name to the client
            Thread update = new Thread(() -> {
                while (true) {
                    StringBuilder allUsers = new StringBuilder("USERLIST,,,");
                    RegisteredUsers.getInstance().getUsers().forEach(e -> allUsers.append(e.getUsername() + "-"));
                    socketOutput.println(allUsers.toString());
                    try {
                        TimeUnit.SECONDS.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            update.start();


            while (true){
                // Split request to header and body
                String[] request = socketInput.readLine().split(",,,"); // Get request

                // Get request header (0th element of request array)
                switch (Headers.valueOf(request[0])) {
                    case GROUPCHAT:
                        // Send message to all by using group chat.
                        ChatController.getInstance().sendToAll(request[1],this.username);
                        break;

                    case PCHAT:
                        ChatController.getInstance().sendToUser(request[1], this.username, request[2]);
                        break;

                    case BUZZ:
                        ChatController.getInstance().sendToUserBuzz(request[1],this.username);
                        break;

                    default:
                        System.out.println("not a good packet");
                        break;

                    case PCHATHIST:
                        ArrayList<String> privateHist = Database.loadHistory(this.username, request[1]);
                        privateHist.forEach(e -> {
                            if (e != null) {
                                socketOutput.println("PCHATHIST,,," + request[1] + ",,," + e);
                            }
                        });
                        break;

                    case GROUPCHATHIST:
                        ArrayList<String> groupHist = Database.loadHistory(this.username, "GROUPCHAT");
                        groupHist.forEach(e -> {
                            if (e != null) {
                                socketOutput.println("GROUPCHATHIST,,," + e);
                            }
                        });
                        break;
                }
            }

        }catch (Exception e){
            System.out.println(username+" with port " + socket.getPort() + " was disconnected at" + LocalDateTime.now());
            PWServerLog.getInstance().pDisconnect(socket,username);
        }
    }

    private void updateClientUsersList ( PrintWriter output) {
        while (true) {
            StringBuilder allUsers = new StringBuilder("USERLIST,,,");
            RegisteredUsers.getInstance().getUsers().forEach(e -> allUsers.append(e.getUsername() + "|"));
            output.println(allUsers);
            try {
                TimeUnit.SECONDS.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
